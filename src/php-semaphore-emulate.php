<?php
/******************************************************************************
 *
 * (C) 2019 by Vyacheslav Sitnikov (vsitnikov@gmail.com)
 *
 ******************************************************************************/

if (!function_exists('sem_get')) {
    @mkdir(__DIR__."/.sem");
    global $emulate_semaphore_locks;
    global $emulate_semaphore_files;
    $emulate_semaphore_locks = $emulate_semaphore_files = [];
    register_shutdown_function(function () {
        global $emulate_semaphore_locks;
        global $emulate_semaphore_files;
        foreach ($emulate_semaphore_locks as $sem_id => $data) {
            @fclose($data['resource']);
            @unlink($data['filename']);
        }
        foreach ($emulate_semaphore_files as $sem_id => $data) {
            @fclose($data['resource']);
            @unlink($data['filename']);
        }
    });
    
    /**
     * @param int $key
     * @param int $max_acquire
     * @param int $perm
     * @param int $auto_release
     *
     * @return bool|resource
     * @see sem_get
     *
     */
    function sem_get(
        $key,
        /** @noinspection PhpUnusedParameterInspection */ $max_acquire = 1,
        /** @noinspection PhpUnusedParameterInspection */ $perm = 0666,
        /** @noinspection PhpUnusedParameterInspection */ $auto_release = 1
    ) {
        if (!is_int($key)) {
            trigger_error("Failed to get. Invalid argument type", E_USER_WARNING);
            return false;
        }
        global $emulate_semaphore_files;
        $filename = __DIR__.'/.sem/'.$key;
        $handle = fopen($filename, 'w+');
        if (false === $handle)
            return false;
        $emulate_semaphore_files[(int)$handle] = ["filename" => $filename, "resource" => $handle];
        return $handle;
    }
    
    /**
     * @param resource $sem_id
     * @param bool     $nowait
     *
     * @return bool
     * @see sem_acquire
     *
     */
    function sem_acquire($sem_id, $nowait = false)
    {
        if (!is_resource($sem_id)) {
            trigger_error("Failed to acquire. Invalid argument type", E_USER_WARNING);
            return false;
        }
        $lock = ($nowait ? LOCK_NB : 0) | LOCK_EX;
        $result = flock($sem_id, $lock);
        if (!$result)
            return false;
        global $emulate_semaphore_locks;
        global $emulate_semaphore_files;
        $emulate_semaphore_locks[(int)$sem_id] = $emulate_semaphore_files[(int)$sem_id];
        return true;
    }
    
    /**
     * @param $sem_id
     *
     * @return bool
     * @see sem_release
     *
     */
    function sem_release($sem_id)
    {
        if (!is_resource($sem_id)) {
            trigger_error("Failed to release. Invalid argument type", E_USER_WARNING);
            return false;
        }
        $result = flock($sem_id, LOCK_UN);
        if ($result) {
            global $emulate_semaphore_locks;
            unset($emulate_semaphore_locks[(int)$sem_id]);
        }
        return $result;
    }
    
    /**
     * @param $sem_id
     *
     * @return bool
     * @see sem_remove
     *
     */
    function sem_remove($sem_id)
    {
        if (!is_resource($sem_id)) {
            trigger_error("Failed to remove. Invalid argument type", E_USER_WARNING);
            return false;
        }
        $result = fclose($sem_id);
        if ($result) {
            global $emulate_semaphore_files;
            @unlink($emulate_semaphore_files[(int)$sem_id]['filename']);
            unset($emulate_semaphore_files[(int)$sem_id]);
        }
        return $result;
    }
}
