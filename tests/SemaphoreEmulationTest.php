<?php

/******************************************************************************
 *
 * (C) 2019 by Vyacheslav Sitnikov (vsitnikov@gmail.com)
 *
 ******************************************************************************/

use PHPUnit\Framework\ExpectationFailedException as PHPUnitException;
use PHPUnit\Framework\TestCase;

/**
 * ETCD class
 *
 * @author    Vyacheslav Sitnikov <vsitnikov@gmail.com>
 * @version   1.0
 * @package   vsitnikov\Etcd
 * @copyright Copyright (c) 2019
 */
final class SemaphoreEmulationTest extends TestCase
{
    
    /**
     *  Функция запуска внешней программы в фоне
     *
     * @param string $cmd Команда для запуска
     */
    private function exec_background($cmd)
    {
        if (strtoupper(substr(PHP_OS, 0, 3)) == "WIN")
            pclose(popen("start /B {$cmd}", "r"));
        else
            exec($cmd." > /dev/null 2>&1 &");
    }
    
    /**
     * Test sem_get function
     */
    public function testSemGet()
    {
        //  Initialize an abstract class, there should be no false parameter
        $sem = sem_get(KEY);
        $sem2 = sem_get(KEY);
        $this->assertIsNumeric(KEY);
        $this->assertIsResource($sem);
        $this->assertIsResource($sem2);
        $this->assertTrue(file_exists(STORAGE."/".KEY));
        @unlink(STORAGE."/".KEY);
        $this->assertTrue(file_exists(STORAGE."/".KEY));
        fclose($sem);
        @unlink(STORAGE."/".KEY);
        $this->assertTrue(file_exists(STORAGE."/".KEY));
        fclose($sem2);
        @unlink(STORAGE."/".KEY);
        $this->assertFalse(file_exists(STORAGE."/".KEY));
        global $emulate_semaphore_files, $emulate_semaphore_locks;
        $emulate_semaphore_files = $emulate_semaphore_locks = [];
    }
    
    /**
     * Test sem_acquire function
     */
    public function testSemAcquire()
    {
        $this->exec_background(escapeshellcmd(PHP_BINARY)." ".escapeshellarg(LOCK_FILE_TEST));
        sleep(1);
        $sem = sem_get(KEY);
        $this->assertIsResource($sem);
        $lock = sem_acquire($sem, true);
        $this->assertFalse($lock);
        $time = time();
        $lock = sem_acquire($sem);
        $time = time() - $time;
        $this->assertTrue($lock);
        $this->assertGreaterThan(1, $time);
        $this->assertLessThan(4, $time);
        sleep(4);
        fclose($sem);
        @unlink(STORAGE."/".KEY);
        $this->assertFalse(file_exists(STORAGE."/".KEY));
        global $emulate_semaphore_files, $emulate_semaphore_locks;
        $emulate_semaphore_files = $emulate_semaphore_locks = [];
    }
    
    /**
     * Test sem_release function
     */
    public function testSemRelease()
    {
        global $emulate_semaphore_files, $emulate_semaphore_locks;
        $sem = sem_get(KEY);
        $this->assertIsResource($sem);
        $this->assertEmpty($emulate_semaphore_locks);
        $lock = sem_acquire($sem);
        $this->assertTrue($lock);
        $this->assertArrayHasKey((int)$sem, $emulate_semaphore_locks);
        $this->assertArrayHasKey((int)$sem, $emulate_semaphore_files);
        $release = sem_release($sem);
        $this->assertTrue($release);
        $this->assertArrayNotHasKey((int)$sem, $emulate_semaphore_locks);
        $this->assertArrayHasKey((int)$sem, $emulate_semaphore_files);
        $this->assertTrue(file_exists(STORAGE."/".KEY));
        fclose($sem);
        @unlink(STORAGE."/".KEY);
        $this->assertFalse(file_exists(STORAGE."/".KEY));
        $emulate_semaphore_files = $emulate_semaphore_locks = [];
    }
    
    /**
     * Test sem_remove function
     */
    public function testSemRemove()
    {
        global $emulate_semaphore_files, $emulate_semaphore_locks;
        $sem = sem_get(KEY);
        $this->assertIsResource($sem);
        $lock = sem_acquire($sem);
        $this->assertTrue($lock);
        $this->assertArrayHasKey((int)$sem, $emulate_semaphore_locks);
        $this->assertArrayHasKey((int)$sem, $emulate_semaphore_files);
        $release = sem_release($sem);
        $this->assertTrue($release);
        $this->assertArrayNotHasKey((int)$sem, $emulate_semaphore_locks);
        $this->assertArrayHasKey((int)$sem, $emulate_semaphore_files);
        $this->assertTrue(file_exists(STORAGE."/".KEY));
        $remove = sem_remove($sem);
        $this->assertTrue($remove);
        $this->assertArrayNotHasKey((int)$sem, $emulate_semaphore_locks);
        $this->assertArrayNotHasKey((int)$sem, $emulate_semaphore_files);
        $this->assertFalse(file_exists(STORAGE."/".KEY));
    }
    
}
